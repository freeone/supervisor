using UnityEngine;
using UnityEngine.UI;

public class TestTool : MonoBehaviour
{
    [SerializeField] private Text _testButtonText;

    private void Awake()
    {
        Application.targetFrameRate = 60;
    }

    private void Start()
    {
        _testButtonText.text = $"x{Time.timeScale}";
    }

    public void OnTestButtonClicked()
    {
        ChangeTimeScale();
    }

    private void ChangeTimeScale()
    {
        if (Time.timeScale < 3.1f) Time.timeScale += 0.5f;
        else Time.timeScale = 1;

        _testButtonText.text = $"x{Time.timeScale}";
    }
}
