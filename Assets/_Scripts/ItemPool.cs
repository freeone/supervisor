using UnityEngine;
using UnityEngine.Pool;

public class ItemPool
{
    public ObjectPool<Item> Pool { get; private set; }
    private Item _prefab;
    private Transform _parent;
    private ItemType _type;
    private Transform _spawnPoint;

    public ItemPool(Item prefab, ItemType type, Transform parent, Transform spawnPoint)
    {
        _prefab = prefab;
        _type = type;
        _parent = parent;
        _spawnPoint = spawnPoint;
        Pool = new ObjectPool<Item>(CreateItem, OnGetItem, OnReleaseItem);
    }

    private Item CreateItem()
    {
        var item = GameObject.Instantiate(_prefab, _parent);
        item.gameObject.SetActive(false);
        item.Init(_type, Pool, _spawnPoint);
        return item;
    }

    private void OnGetItem(Item item)
    {
        item.Spawn();
    }

    private void OnReleaseItem(Item item)
    {
        item.gameObject.SetActive(false);
    }
}
