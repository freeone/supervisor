using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TaskDisplay : MonoBehaviour
{
    [SerializeField] private List<GameObject> _leftListEntries;
    [SerializeField] private List<GameObject> _rightListEntries;

    [SerializeField] private Color _defaultColor;
    [SerializeField] private Color _completedColor;

    private List<EntryContent> _leftListContent;
    private List<EntryContent> _rightListContent;
    private List<ItemType> _leftListTypes;
    private List<ItemType> _rightListTypes;

    public static TaskDisplay Instance { get; private set; }

    private void Awake()
    {
        _leftListContent = new List<EntryContent>();
        _rightListContent = new List<EntryContent>();
        _leftListTypes = new List<ItemType>();
        _rightListTypes = new List<ItemType>();


        InitializeListContent(_leftListEntries, _leftListContent);
        InitializeListContent(_rightListEntries, _rightListContent);

        Instance = this;
    }

    public void ShowTaskList(Dictionary<ItemType, int> tasks, Side side)
    {
        int i = 0;
        List<GameObject> listEntries = _leftListEntries;
        List<EntryContent> entryContent = _leftListContent;
        List<ItemType> listTypes = _leftListTypes;

        if (side == Side.Right)
        {
            listEntries = _rightListEntries;
            entryContent = _rightListContent;
            listTypes = _rightListTypes;
        }

        listTypes.Clear();

        HideAllListEntries(listEntries);

        foreach (var task in tasks)
        {
            listEntries[i].SetActive(true);
            listTypes.Add(task.Key);
            entryContent[i].Image.sprite = ItemSpawner.Instance.Icons[task.Key];
            entryContent[i].Text.text = $"0 / {task.Value}";
            i++;
        }
    }

    public void UpdateNumber(Side side, ItemType type, int collected, int target)
    {
        int i;
        if (side == Side.Left)
        {
            i = _leftListTypes.IndexOf(type);
            _leftListContent[i].Text.text = $"{collected} / {target}";
        }
        else
        {
            i = _rightListTypes.IndexOf(type);
            _rightListContent[i].Text.text = $"{collected} / {target}";
        }
    }

    private void InitializeListContent(List<GameObject> listEntries, List<EntryContent> listContent)
    {
        foreach (var entry in listEntries)
        {
            listContent.Add(new EntryContent(entry.GetComponentInChildren<Image>(true), entry.GetComponentInChildren<Text>(true)));
        }
    }

    public void SwitchColorToCompleted(Side side, bool tasksCompleted)
    {
        List<EntryContent> listContent = side == Side.Left ? _leftListContent : _rightListContent;
        Color color = tasksCompleted ? _completedColor : _defaultColor;

        foreach (var entry in listContent)
        {
            entry.Text.color = color;
        }
    }

    private void HideAllListEntries(List<GameObject> listEntries)
    {
        foreach (var entry in listEntries)
        {
            entry.SetActive(false);
        }
    }
}

public class EntryContent
{
    public Image Image { get; private set; }
    public Text Text { get; private set; }

    public EntryContent(Image image, Text text)
    {
        Image = image;
        Text = text;
    }
}
