using System.Collections.Generic;

public class GameDataManager
{
    private GameData _gameData;
    public GameData GameData => _gameData;

    private const int MAX_SCORE_LINES = 20;

    public GameDataManager()
    {
        InitGameData();
    }

    private void InitGameData()
    {
        _gameData = Saver.GetSavedData();
    }

    public bool CheckNewRecord(int newScore)
    {
        if (newScore == 0)
            return false;

        if (_gameData.Scores.Count == 0 || newScore > _gameData.Scores[0])
            return true;
        else
            return false;
    }

    public bool TryAddNewScore(int newScore)
    {
        if (newScore == 0)
            return false;

        if (_gameData.Scores.Count < MAX_SCORE_LINES)
        {
            AddScore(newScore);
            return true;
        }
        else
        {
            if (newScore > _gameData.Scores[MAX_SCORE_LINES - 1])
            {
                _gameData.Scores.RemoveAt(MAX_SCORE_LINES - 1);
                AddScore(newScore);
                return true;
            }
        }

        return false;
    }

    private void AddScore(int newScore)
    {
        _gameData.Scores.Add(newScore);
        _gameData.Scores.Sort();
        _gameData.Scores.Reverse();
    }

    public void SaveData()
    {
        Saver.SaveGameData(_gameData);
    }

}

