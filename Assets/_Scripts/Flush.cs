using UnityEngine;

public class Flush : MonoBehaviour
{
    [SerializeField] private Animation _leftHolderAnimation;
    [SerializeField] private Animation _rightHolderAnimation;

    public void FlushOne(Side side)
    {
        if (side == Side.Left) _leftHolderAnimation.Play();
        else _rightHolderAnimation.Play();
    }

    public void FlushBoth()
    {
        FlushOne(Side.Left);
        FlushOne(Side.Right);
    }
}
