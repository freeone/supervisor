using UnityEngine;

public class PistonMagnet : MonoBehaviour
{
    [SerializeField] private Side _side;
    [SerializeField] private float _magnetForce;
    [SerializeField] private float _disableDistance;
    [SerializeField] private Transform _realMagnetCenter;

    private void OnTriggerStay(Collider other)
    {
        if (AmIOnMoving() && other.TryGetComponent(out Item item))
        {
            if (GameManager.Instance.TaskCenter.IsItemNeeded(_side, item.ItemType))
            {
                //item.PullMe((_realMagnetCenter.position - item.transform.position) * _magnetForce * Time.fixedDeltaTime); // todo realcenter is unused
                item.PullMe((transform.position - item.transform.position) * _magnetForce * Time.fixedDeltaTime);
            }
        }
    }

    private bool AmIOnMoving()
    {
        if (_side == Side.Left)
            return PistonController.Instance.IsLeftMoving;
        else
            return PistonController.Instance.IsRightMoving;
    }
}
