public abstract class Bonus
{
    public abstract float Duration { get; }
    public abstract BonusType Type { get; }

    public abstract void ApplyBonus();
    public abstract void ClearBonusEffect();
}

public enum BonusType
{
    SlowBelt, IncreaseChance, Magnet, ShowNeeded
}