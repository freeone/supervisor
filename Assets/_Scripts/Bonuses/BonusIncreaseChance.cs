public class BonusIncreaseChance : Bonus
{
    public override float Duration => 8f;
    public override BonusType Type => BonusType.IncreaseChance;

    private float _bonusValue = 0.4f;
    private float _defaultChanceBonus;

    public BonusIncreaseChance()
    {
        _defaultChanceBonus = ItemSpawner.Instance.WantedPropabilityBonus;
    }

    public override void ApplyBonus()
    {
        var valueToAdd = (1 - _defaultChanceBonus) * _bonusValue;
        ItemSpawner.Instance.WantedPropabilityBonus = _defaultChanceBonus + valueToAdd;
    }

    public override void ClearBonusEffect()
    {
        ItemSpawner.Instance.WantedPropabilityBonus = _defaultChanceBonus;
    }
}
