using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class BonusProcessor
{
    private static Dictionary<BonusType, Bonus> _bonuses;

    public static event Action<bool> ParticlesBonusSwitched;
    public static event Action ItemCollected;
    public static bool CurrentParticlesState { get; private set; }

    public static void Init(GameObject leftMagnet, GameObject rightMagnet)
    {
        _bonuses = new Dictionary<BonusType, Bonus>();
        CurrentParticlesState = false;

        _bonuses.Add(BonusType.SlowBelt, new BonusSlowBelt());
        _bonuses.Add(BonusType.IncreaseChance, new BonusIncreaseChance());
        _bonuses.Add(BonusType.Magnet, new BonusMagnet(leftMagnet, rightMagnet));
        _bonuses.Add(BonusType.ShowNeeded, new BonusShowNeeded());
        //add all types
    }

    public static IEnumerator UseBonus(BonusType type)
    {
        _bonuses[type].ApplyBonus();

        yield return new WaitForSeconds(_bonuses[type].Duration);

        _bonuses[type].ClearBonusEffect();
    }

    public static void InvokeParticleEvent(bool state)
    {
        CurrentParticlesState = state;
        ParticlesBonusSwitched.Invoke(state);
    }

    public static void InvokeItemCollected()
    {
        ItemCollected.Invoke();
    }
}
