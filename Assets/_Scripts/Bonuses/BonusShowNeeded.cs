public class BonusShowNeeded : Bonus
{
    public override float Duration => 30f; // todo: make it lower
    public override BonusType Type => BonusType.ShowNeeded;

    public BonusShowNeeded()
    {
    }

    public override void ApplyBonus()
    {
        BonusProcessor.InvokeParticleEvent(true);
    }

    public override void ClearBonusEffect()
    {
        BonusProcessor.InvokeParticleEvent(false);
    }
}
