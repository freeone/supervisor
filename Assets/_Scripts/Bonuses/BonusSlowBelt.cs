public class BonusSlowBelt : Bonus
{
    public override float Duration => 5f;
    public override BonusType Type => BonusType.SlowBelt;

    private float _defaultBeltSpeed;
    private float _bonusMultiplier = 0.4f;

    public BonusSlowBelt()
    {
        _defaultBeltSpeed = Belts.Instance.MainBeltForwardSpeed;
    }

    public override void ApplyBonus()
    {
        Belts.Instance.SetMainForwardSpeed(_defaultBeltSpeed * _bonusMultiplier);
    }

    public override void ClearBonusEffect()
    {
        Belts.Instance.SetMainForwardSpeed(_defaultBeltSpeed);
    }
}
