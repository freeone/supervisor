using UnityEngine;

public class BonusMagnet : Bonus
{
    public override float Duration => 18f; //todo balance
    public override BonusType Type => BonusType.Magnet;

    private GameObject _leftMagnet;
    private GameObject _rightMagnet;

    public BonusMagnet(GameObject leftMagnet, GameObject rightMagnet)
    {
        _leftMagnet = leftMagnet;
        _rightMagnet = rightMagnet;
    }

    public override void ApplyBonus()
    {
        _leftMagnet.SetActive(true);
        _rightMagnet.SetActive(true);
    }

    public override void ClearBonusEffect()
    {
        _leftMagnet.SetActive(false);
        _rightMagnet.SetActive(false);
    }
}
