using System.Collections.Generic;

public class GameData
{
    public List<int> Scores;

    public int Coins;

    public GameData()
    {
        Scores = new List<int>();
        Coins = 0;
    }

    public void AddCoins(int coinsToAdd)
    {
        Coins += coinsToAdd;
    }
}