using System;
using System.Collections.Generic;

public class TaskCenter
{
    public Dictionary<ItemType, int> LeftStorageTasks { get; private set; }
    public Dictionary<ItemType, int> LeftStorageCollected { get; private set; }

    public Dictionary<ItemType, int> RightStorageTasks { get; private set; }
    public Dictionary<ItemType, int> RightStorageCollected { get; private set; }

    public List<ItemType> NeededTypes { get; private set; }

    private int minTaskNumber = 1;
    private int maxTaskNumber = 3; //exclusive
    private int minItemNumber = 1;
    private int maxItemNumber = 3; //exclusive

    public void InitializeTaskLists()
    {
        LeftStorageTasks = new Dictionary<ItemType, int>();
        RightStorageTasks = new Dictionary<ItemType, int>();
        LeftStorageCollected = new Dictionary<ItemType, int>();
        RightStorageCollected = new Dictionary<ItemType, int>();
        NeededTypes = new List<ItemType>();
    }

    public bool IsItemNeeded(Side side, ItemType type)
    {
        if (side == Side.Left)
            return LeftStorageTasks.ContainsKey(type) && LeftStorageTasks[type] > LeftStorageCollected[type];
        else
            return RightStorageTasks.ContainsKey(type) && RightStorageTasks[type] > RightStorageCollected[type];
    }

    public void ResetAllTasks()
    {
        ResetSideTasks(Side.Left);
        ResetSideTasks(Side.Right);
    }

    public void ResetSideTasks(Side side)
    {
        if (side == Side.Left)
        {
            LeftStorageTasks.Clear();
            LeftStorageCollected.Clear();
            GenerateTaskList(Side.Left);
        }
        else
        {
            RightStorageTasks.Clear();
            RightStorageCollected.Clear();
            GenerateTaskList(Side.Right);
        }
    }

    public void ReFillNeededTypesList()
    {
        NeededTypes.Clear();

        foreach (var task in LeftStorageTasks)
        {
            if (!NeededTypes.Contains(task.Key) && LeftStorageCollected[task.Key] < LeftStorageTasks[task.Key])
                NeededTypes.Add(task.Key);
        }

        foreach (var task in RightStorageTasks)
        {
            if (!NeededTypes.Contains(task.Key) && RightStorageCollected[task.Key] < RightStorageTasks[task.Key]) 
                NeededTypes.Add(task.Key);
        }
    }

    public void GenerateTaskList(Side side)
    {
        int taskNumber = UnityEngine.Random.Range(minTaskNumber, maxTaskNumber);
        for (int i = 0; i < taskNumber; i++)
        {
            if (side == Side.Left) GenerateTask(LeftStorageTasks, LeftStorageCollected);
            else GenerateTask(RightStorageTasks, RightStorageCollected);
        }

        ReFillNeededTypesList();
    }

    public void CheckSideDrop(Side side, Item item)
    {
        Dictionary<ItemType, int> storageTasks = LeftStorageTasks;
        Dictionary<ItemType, int> itemsCollected = LeftStorageCollected;
        if (side == Side.Right)
        {
            storageTasks = RightStorageTasks;
            itemsCollected = RightStorageCollected;
        }

        foreach (var task in storageTasks)
        {
            if (item.ItemType == task.Key)
            {
                if (itemsCollected[item.ItemType] < task.Value)
                {
                    itemsCollected[item.ItemType]++;
                    TaskDisplay.Instance.UpdateNumber(side, item.ItemType, itemsCollected[item.ItemType], storageTasks[item.ItemType]);
                    BonusProcessor.InvokeItemCollected();

                    if (itemsCollected[item.ItemType] == storageTasks[item.ItemType])
                    {
                        if (CheckOrderCompleted(side)) GameManager.Instance.SendOrder(side);
                    }
                    return;
                }
                else
                {
                    GameManager.Instance.LoseGame(LoseReason.TooMany, item.ItemType, side);
                    return;
                }
            }
        }
        GameManager.Instance.LoseGame(LoseReason.WrongType, item.ItemType, side);
    }

    private bool CheckOrderCompleted(Side side)
    {
        Dictionary<ItemType, int> storageTasks = LeftStorageTasks;
        Dictionary<ItemType, int> itemsCollected = LeftStorageCollected;
        if (side == Side.Right)
        {
            storageTasks = RightStorageTasks;
            itemsCollected = RightStorageCollected;
        }

        foreach (var task in storageTasks)
        {
            if (task.Value != itemsCollected[task.Key]) return false;
        }

        return true;
    }

    public void CheckExitItem(Side side, Item item)
    {
        Dictionary<ItemType, int> storageTasks = LeftStorageTasks;
        Dictionary<ItemType, int> itemsCollected = LeftStorageCollected;
        if (side == Side.Right)
        {
            storageTasks = RightStorageTasks;
            itemsCollected = RightStorageCollected;
        }
        foreach (var task in storageTasks)
        {
            if (item.ItemType == task.Key)
            {
                if (itemsCollected[item.ItemType] > 0)
                {
                    itemsCollected[item.ItemType]--;
                    TaskDisplay.Instance.UpdateNumber(side, item.ItemType, itemsCollected[item.ItemType], storageTasks[item.ItemType]);
                    return;
                }
            }
        }
    }

    public void CheckCenterDrop(Item item)
    {
        if (LeftStorageCollected.ContainsKey(item.ItemType) && LeftStorageCollected[item.ItemType] < LeftStorageTasks[item.ItemType])
        {
            GameManager.Instance.LoseGame(LoseReason.DroppedNeeded, item.ItemType);
            return;
        }
        if (RightStorageCollected.ContainsKey(item.ItemType) && RightStorageCollected[item.ItemType] < RightStorageTasks[item.ItemType])
        {
            GameManager.Instance.LoseGame(LoseReason.DroppedNeeded, item.ItemType);
            return;
        }
    }

    private void GenerateTask(Dictionary<ItemType, int> storageTasks, Dictionary<ItemType, int> collectedItems)
    {
        ItemType type;
        do
        {
            type = (ItemType)UnityEngine.Random.Range(0, Enum.GetNames(typeof(ItemType)).Length - 1);
        }
        while (storageTasks.ContainsKey(type));
        int number = UnityEngine.Random.Range(minItemNumber, maxItemNumber);

        storageTasks.Add(type, number);
        collectedItems.Add(type, 0);
    }

    public (int, int) GetTaskInfo(Side side)
    {
        int lines = 0;
        int totalCount = 0;

        if (side == Side.Left)
        {
            lines = LeftStorageTasks.Count;
            foreach (var line in LeftStorageTasks)
            {
                totalCount += line.Value;
            }
        }
        else
        {
            lines = RightStorageTasks.Count;
            foreach (var line in RightStorageTasks)
            {
                totalCount += line.Value;
            }
        }

        return (lines, totalCount);
    }
}

public enum Side
{
    Left, Right
}