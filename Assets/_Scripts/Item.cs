using UnityEngine;
using UnityEngine.Pool;

[RequireComponent(typeof(Rigidbody))]
public class Item : MonoBehaviour
{
    [SerializeField] private ItemType _itemType;
    [SerializeField] private Sprite _icon;
    [SerializeField] private ParticlesManager _particlesManager;

    private Color LEFT_SIDE_PARTICLES = Color.green;
    private Color RIGHT_SIDE_PARTICLES = Color.red;

    public ItemType ItemType => _itemType;
    public Sprite Icon => _icon;

    private Rigidbody _rigidbody;
    private ObjectPool<Item> _pool;
    private Transform _spawnPoint;
    private bool _collected = false;

    public void Init(ItemType type, ObjectPool<Item> pool, Transform spawnPoint)
    {
        _itemType = type;
        _pool = pool;
        _spawnPoint = spawnPoint;
        _rigidbody = GetComponent<Rigidbody>();

        BonusProcessor.ParticlesBonusSwitched += SwitchParticles;
        GameManager.Instance.OrderReCreated += CheckParticlesState;
        BonusProcessor.ItemCollected += CheckParticlesState;
    }

    public void OnDestroy()
    {
        BonusProcessor.ParticlesBonusSwitched -= SwitchParticles;
        GameManager.Instance.OrderReCreated -= CheckParticlesState;
        BonusProcessor.ItemCollected -= CheckParticlesState;
    }

    public void Spawn()
    {
        SetStartPosition();
        SetStartRotation();
        _rigidbody.velocity = Vector3.zero;
        transform.SetParent(_spawnPoint);
        gameObject.SetActive(true);
        _collected = false;
        CheckParticlesState();
    }

    private void SetStartPosition()
    {
        transform.position = _spawnPoint.position + Vector3.right * Random.Range(-0.15f, 0.15f);
    }

    private void SetStartRotation()
    {
        transform.rotation = Quaternion.Euler(Random.Range(-30, 30), Random.Range(-30, 30), Random.Range(-30, 30));
    }

    public void Release()
    {
        _pool.Release(this);
    }

    public void PullMe(Vector3 pullingForce)
    {
        _rigidbody.AddForce(pullingForce, ForceMode.Force);
    }

    #region Particles

    private void SwitchParticles(bool state)
    {
        _particlesManager.SetRotatorState(state);

        if (state)
        {
            CheckParticlesForSide(Side.Left, true);
            CheckParticlesForSide(Side.Right, true);
        }
        else
        {
            _particlesManager.FullStop();
        }
    }

    private void CheckParticlesState()
    {
        CheckParticlesForSide(Side.Left);
        CheckParticlesForSide(Side.Right);
    }

    private void CheckParticlesForSide(Side side, bool knownState = false)
    {
        if ((knownState ? true : BonusProcessor.CurrentParticlesState) && !_collected && GameManager.Instance.TaskCenter.IsItemNeeded(side, _itemType))
        {
            _particlesManager.Play(side);
        }
        else
        {
            _particlesManager.ClearAndStop(side);
        }
    }

    public void OnCollected()
    {
        _collected = true;
        _particlesManager.FullStop();
    }

    #endregion
}

public enum ItemType
{
    Banana, Chipsbag, Mushroom, Pineapple, Sausage, Pepper, DoesNotMatter = 255
}