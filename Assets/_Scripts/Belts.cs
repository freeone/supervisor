using System.Collections;
using UnityEngine;

public class Belts : MonoBehaviour
{
    [SerializeField] private float _mainBeltForwardSpeed;
    public float MainBeltForwardSpeed => _mainBeltForwardSpeed;

    [SerializeField] private float _mainBeltBackwardSpeed;
    [SerializeField] private Material _conveyor;
    [SerializeField] private Material _conveyorRotated;
    [SerializeField] private Material _leftBelt;
    [SerializeField] private Material _rightBelt;
    [SerializeField] private Material _lowerBelts;
    [SerializeField] private Material _openBoxBelt;
    [SerializeField] private Material _packedBoxBelt;
    // probably should get mats in Awake

    [SerializeField] private float _newBoxDistanationZ;
    [SerializeField] private float _lowerBeltsSpeed;
    [SerializeField] private float _boxSpeed;

    [SerializeField] private Rigidbody _mainBeltRB;
    [SerializeField] private Rigidbody _leftBeltRB;
    [SerializeField] private Rigidbody _rightBeltRB;
    [SerializeField] private Rigidbody _leftLowerBeltRB;
    [SerializeField] private Rigidbody _rightLowerBeltRB;
    [SerializeField] private Rigidbody _openBoxBeltRB;
    [SerializeField] private Rigidbody _packedBoxBeltRB;

    private Vector3 _stablePosition;

    private float _mainBeltSpeed;
    private float _leftBeltSpeed;
    private float _rightBeltSpeed;

    private Vector2 _offsetMain;
    private Vector2 _offsetMainRotated;
    private Vector2 _offsetLeft;
    private Vector2 _offsetRight;
    private Vector2 _offsetLowers;
    private Vector2 _offsetOpenBox;
    private Vector2 _offsetPackedBox;

    public static Belts Instance { get; private set; }

    private void Awake()
    {
        Instance = this;

        _mainBeltSpeed = _mainBeltForwardSpeed;
        _leftBeltSpeed = 0;
        _rightBeltSpeed = 0;

        _offsetMain = new Vector2(0, 7.73f);
        _offsetMainRotated = new Vector2(1.47f, 0);
        _offsetLeft = Vector2.zero;
        _offsetRight = Vector2.zero;
        _offsetLowers = Vector2.zero;
        _offsetOpenBox = Vector2.zero;
        _offsetPackedBox = Vector2.zero;
    }



    private void FixedUpdate()
    {
        if (_mainBeltSpeed != 0) MoveBelt(Belt.Main);
        if (_leftBeltSpeed != 0) MoveBelt(Belt.Left);
        if (_rightBeltSpeed != 0) MoveBelt(Belt.Right);
        if (_lowerBeltsSpeed != 0)
        {
            MoveBelt(Belt.LowerLeft);
            MoveBelt(Belt.LowerRight);
        }
        if (_boxSpeed != 0)
        {
            MoveBelt(Belt.OpenBox);
            MoveBelt(Belt.PackedBox);
        }
    }

    public void MoveMainBeltForward()
    {
        _mainBeltSpeed = _mainBeltForwardSpeed;
    }

    public void MoveMainBeltBackward()
    {
        _mainBeltSpeed = _mainBeltBackwardSpeed;
    }

    public void SetMainForwardSpeed(float speed)
    {
        _mainBeltForwardSpeed = speed;
        if (_mainBeltSpeed < 0)
            _mainBeltSpeed = _mainBeltForwardSpeed;
    }

    /*public void SetMainBeltSpeed(Belt belt, float speed)
    {
        switch (belt)
        {
            case Belt.Main:
                _mainBeltSpeed = speed;
                break;
            case Belt.Left:
                _leftBeltSpeed = speed;
                break;
            case Belt.Right:
                _rightBeltSpeed = speed;
                break;
            case Belt.LowerLeft:
            case Belt.LowerRight:
                _lowerBeltsSpeed = speed;
                break;
            case Belt.OpenBox:
            case Belt.PackedBox:
                _boxSpeed = speed;
                break;
        }
    }*/

    private void MoveBelt(Belt belt)
    {
        Rigidbody rb = _mainBeltRB;
        float speed = 0;

        switch (belt)
        {
            case Belt.Main:
                rb = _mainBeltRB;
                speed = _mainBeltSpeed;
                break;
            case Belt.Left:
                rb = _leftBeltRB;
                speed = _leftBeltSpeed;
                break;
            case Belt.Right:
                rb = _rightBeltRB;
                speed = _rightBeltSpeed;
                break;
            case Belt.LowerLeft:
                rb = _leftLowerBeltRB;
                speed = _lowerBeltsSpeed;
                break;
            case Belt.LowerRight:
                rb = _rightLowerBeltRB;
                speed = _lowerBeltsSpeed;
                break;
            case Belt.OpenBox:
                rb = _openBoxBeltRB;
                speed = _boxSpeed;
                break;
            case Belt.PackedBox:
                rb = _packedBoxBeltRB;
                speed = _boxSpeed;
                break;
        }

        _stablePosition = rb.position;
        rb.position -= rb.transform.forward * speed * Time.fixedDeltaTime;
        rb.MovePosition(_stablePosition);

        if (belt != Belt.LowerRight) MoveTexture(belt); // don't move txt twice
    }

    private void MoveTexture(Belt belt)
    {
        switch (belt)
        {
            case Belt.Main:
                _offsetMain.y += _mainBeltSpeed * 0.65f * Time.fixedDeltaTime;
                _conveyor.mainTextureOffset = _offsetMain;
                _offsetMainRotated.x += _mainBeltSpeed * -0.65f * Time.fixedDeltaTime;
                _conveyorRotated.mainTextureOffset = _offsetMainRotated;
                break;
            case Belt.Left:
                _offsetLeft.y += _leftBeltSpeed * 0.65f * Time.fixedDeltaTime;
                _leftBelt.mainTextureOffset = _offsetLeft;
                break;
            case Belt.Right:
                _offsetRight.y += _rightBeltSpeed * 0.65f * Time.fixedDeltaTime;
                _rightBelt.mainTextureOffset = _offsetRight;
                break;
            case Belt.LowerLeft:
                _offsetLowers.y += _lowerBeltsSpeed * 0.65f * Time.fixedDeltaTime;
                _lowerBelts.mainTextureOffset = _offsetLowers;
                break;
            case Belt.OpenBox:
                _offsetOpenBox.y += _boxSpeed * 0.65f * Time.fixedDeltaTime;
                _openBoxBelt.mainTextureOffset = _offsetOpenBox;
                break;
            case Belt.PackedBox:
                _offsetPackedBox.y += _boxSpeed * 0.65f * Time.fixedDeltaTime;
                _packedBoxBelt.mainTextureOffset = _offsetPackedBox;
                break;
        }
        
    }

    public void DeliverBox(Transform box, Side side)
    {
        if (side == Side.Left) _leftBeltSpeed = -2f;
        else if (side == Side.Right) _rightBeltSpeed = -2f;

        StartCoroutine(DeliveringBox(box, side));
    }

    private IEnumerator DeliveringBox(Transform box, Side side)
    {
        while (box.position.z < _newBoxDistanationZ)
        {
            yield return null;
        }

        if (side == Side.Left) _leftBeltSpeed = 0;
        else if (side == Side.Right) _rightBeltSpeed = 0;
    }
}

public enum Belt
{
    Main, Left, Right, LowerLeft, LowerRight, OpenBox, PackedBox
}