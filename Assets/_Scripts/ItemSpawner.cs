using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class ItemSpawner : MonoBehaviour
{
    [SerializeField] private float _spawnPeriodSeconds;
    [SerializeField] private Transform _spawnPoint;
    [SerializeField] private Transform _leftEmptyBoxSpawnPoint;
    [SerializeField] private Transform _rightEmptyBoxSpawnPoint;
    [SerializeField] private Transform _packedBoxSpawnPoint;

    [SerializeField] private SafeSpawnArea _safeArea;

    [SerializeField] private Transform _emptyBoxPrefab;
    [SerializeField] private Transform _packedBoxPrefab;
    [SerializeField] private List<Item> _itemPrefabs;

    public float WantedPropabilityBonus { get; set; }
    private const float DEFAULT_WANTED_BONUS = 0.25f;

    public Dictionary<ItemType, Sprite> Icons { get; private set; }
    private WaitForSeconds _spawnPeriod;
    private Dictionary<ItemType, ItemPool> _itemPools;

    private List<ItemType> _totalTypes;
    private List<ItemType> _itemsWanted;
    private List<ItemType> _itemsUnwanted;
    private float _basicChance;
    private float _wantedTotalChance;
    private float _unwantedTotalChance;
    private float _chance;
    private int _typeToSpawn;

    public static ItemSpawner Instance { get; private set; }

    private void Awake()
    {
        WantedPropabilityBonus = DEFAULT_WANTED_BONUS;
        Instance = this;

        _itemPools = new Dictionary<ItemType, ItemPool>();
        Icons = new Dictionary<ItemType, Sprite>();
        for (int i = 0; i < _itemPrefabs.Count; i++)
        {
            _itemPools.Add(_itemPrefabs[i].ItemType, new ItemPool(_itemPrefabs[i], _itemPrefabs[i].ItemType, transform, _spawnPoint));

            if (_itemPrefabs[i].Icon != null) Icons.Add(_itemPrefabs[i].ItemType, _itemPrefabs[i].Icon);
        }

        _spawnPeriod = new WaitForSeconds(_spawnPeriodSeconds);

        _totalTypes = Enum.GetValues(typeof(ItemType)).Cast<ItemType>().ToList();
        _totalTypes.Remove(ItemType.DoesNotMatter);
    }

    public void StartSpawner()
    {
        StartCoroutine(PeriodicSpawn());

        _itemsWanted = GameManager.Instance.TaskCenter.NeededTypes;
        _basicChance = 1f / _totalTypes.Count;
    }

    public void ReleaseAllAtSpawnPoint()
    {
        foreach (var item in _spawnPoint.GetComponentsInChildren<Item>())
        {
            item.Release();
        }
        _safeArea.Clear();
    }

    private IEnumerator PeriodicSpawn()
    {
        while (true)
        {
            yield return _spawnPeriod;
            if (_safeArea.IsEmpty())
            {
                if (_itemsWanted.Count > 0)
                {
                    GameManager.Instance.TaskCenter.ReFillNeededTypesList();
                    GenerateProbabilities();

                    _chance = Random.Range(0f, 1f);
                    if (_chance < _wantedTotalChance)
                    {
                        _typeToSpawn = Mathf.FloorToInt(_chance / (_wantedTotalChance / _itemsWanted.Count));
                        _itemPools[_itemsWanted[_typeToSpawn]].Pool.Get();
                    }
                    else
                    {
                        _typeToSpawn = Mathf.FloorToInt((_chance - _wantedTotalChance) / (_unwantedTotalChance / _itemsUnwanted.Count));
                        _itemPools[_itemsUnwanted[_typeToSpawn]].Pool.Get();
                    }
                }
                else
                {
                    var type = (ItemType)Random.Range(0, Enum.GetNames(typeof(ItemType)).Length - 1);
                    _itemPools[type].Pool.Get();
                }
                
            }
        }
    }

    // is used before each spawn
    private void GenerateProbabilities()
    {
        _itemsUnwanted = _totalTypes.Except(_itemsWanted).ToList();
        _wantedTotalChance = _basicChance * _itemsWanted.Count;
        _unwantedTotalChance = 1 - _wantedTotalChance;
        _wantedTotalChance += _unwantedTotalChance * WantedPropabilityBonus;
        _unwantedTotalChance -= _unwantedTotalChance * WantedPropabilityBonus;
        Debug.Log($"wanted sum: {_wantedTotalChance}, unwanted: {_unwantedTotalChance}");
    }

    public Transform SpawnEmptyBox(Side side)
    {
        Transform spawnPoint = side == Side.Left ? _leftEmptyBoxSpawnPoint : _rightEmptyBoxSpawnPoint;
        return GameObject.Instantiate(_emptyBoxPrefab, spawnPoint.position, spawnPoint.rotation, spawnPoint);
    }

    public void SpawnPackedBox()
    {
        GameObject.Instantiate(_packedBoxPrefab, _packedBoxSpawnPoint.position, Quaternion.Euler(0, Random.Range(-150, -210), 0), _packedBoxSpawnPoint);
    }
}