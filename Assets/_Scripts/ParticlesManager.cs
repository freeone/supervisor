using UnityEngine;

public class ParticlesManager : MonoBehaviour
{
    [SerializeField] private ParticleSystem _leftParticles;
    [SerializeField] private ParticleSystem _rightParticles;
    [SerializeField] private ParticleSystemRotator _rotator;

    public void SetRotatorState(bool state)
    {
        _rotator.enabled = state;
    }

    public void FullStop()
    {
        ClearAndStop(Side.Left);
        ClearAndStop(Side.Right);
    }

    public void Play(Side side)
    {
        if (side == Side.Left)
            _leftParticles.Play();
        else
            _rightParticles.Play();
    }

    public void ClearAndStop(Side side)
    {
        if (side == Side.Left)
        {
            _leftParticles.Clear();
            _leftParticles.Stop();
        }
        else
        {
            _rightParticles.Clear();
            _rightParticles.Stop();
        }
    }

}
