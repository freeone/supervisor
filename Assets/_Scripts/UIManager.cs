using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField] private GameObject _menuPanel;
    [SerializeField] private Text _coinsCount;

    [SerializeField] private GameObject _gamePanel;

    [SerializeField] private GameObject _losePanel;
    [SerializeField] private Text _loseReasonText;
    [SerializeField] private Text _resultCoinText;
    [SerializeField] private Text _resultOrderText;
    [SerializeField] private Color _defaultOrderTextColor;
    [SerializeField] private Color _recordOrderTextColor;

    private List<GameObject> _panels;

    public static UIManager Instance { get; private set; }

    private void Awake()
    {
        Instance = this;

        _panels = new List<GameObject> { _menuPanel, _gamePanel, _losePanel };
    }

    public void ChangeScreen(UIScreen target, string loseReason = null)
    {
        foreach (var screen in _panels)
        {
            screen.SetActive(false);
        }

        switch (target)
        {
            case UIScreen.Game:
                _gamePanel.SetActive(true);
                break;
            case UIScreen.Menu:
                _menuPanel.SetActive(true);
                break;
            case UIScreen.GameOver:
                _losePanel.SetActive(true);
                _loseReasonText.text = loseReason;
                break;
        }
    }

    public void OnStartClicked()
    {
        ChangeScreen(UIScreen.Game);
        GameManager.Instance.StartGame();
    }

    public void OnMenuClicked()
    {
        ChangeScreen(UIScreen.Menu);
        GameManager.Instance.OpenMenu();
    }

    public void OnRetryClicked()
    {
        ChangeScreen(UIScreen.Game);
        GameManager.Instance.RestartGame();
    }

    public void LoseGame(LoseReason reason, ItemType type, RewardCenter rewardCenter, ContainerColor containerColorer, bool newRecord, Side side = Side.Left)
    {
        if (reason == LoseReason.WrongType)
        {
            Instance.ChangeScreen(UIScreen.GameOver, $"No {type} ordered!");
            containerColorer.ChangeContainerColor(side, ColorState.Fail);
        }
        else if (reason == LoseReason.TooMany)
        {
            Instance.ChangeScreen(UIScreen.GameOver, $"Too many {type}s!");
            containerColorer.ChangeContainerColor(side, ColorState.Fail);
        }
        else if (reason == LoseReason.DroppedNeeded)
            Instance.ChangeScreen(UIScreen.GameOver, $"You needed that {type}!");

        _resultCoinText.text = $"coins\n{rewardCenter.CurrentCoins}";
        _resultOrderText.text = $"orders\n{rewardCenter.CurrentBoxes}";

        if (newRecord)
            _resultOrderText.color = _recordOrderTextColor;
        else
            _resultOrderText.color = _defaultOrderTextColor;

        //Debug.Log($"You've completed {rewardCenter.CurrentBoxes} orders and got {rewardCenter.CurrentCoins} coins");
    }

    public void SetCoinsTextMainMenu(int coinsCount)
    {
        _coinsCount.text = coinsCount.ToString();
    }
}

public enum UIScreen
{
    Menu, Game, GameOver
}