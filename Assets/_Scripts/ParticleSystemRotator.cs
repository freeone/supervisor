using UnityEngine;

public class ParticleSystemRotator : MonoBehaviour
{
    private Quaternion _targetRotation;
    private void Start()
    {
        _targetRotation = Quaternion.Euler(-90, 0, 0);
    }
    void Update()
    {
        transform.rotation = _targetRotation;
    }
}
