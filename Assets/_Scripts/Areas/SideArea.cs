using UnityEngine;

public class SideArea : MonoBehaviour
{
    [SerializeField] private Side _side;
    [SerializeField] private Transform _parent;

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Item>(out Item item))
        {
            GameManager.Instance.TaskCenter.CheckSideDrop(_side, item);
            item.transform.SetParent(_parent);
            item.OnCollected();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.TryGetComponent<Item>(out Item item))
            if (!GameManager.Instance.SendingOrderStates[_side])
                GameManager.Instance.TaskCenter.CheckExitItem(_side, item);
    }
}