using UnityEngine;

public class LostItemsArea : MonoBehaviour
{
    [SerializeField] private Transform _leftAreaTransform;
    [SerializeField] private Transform _rightAreaTransform;

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Item>(out Item item))
        {
            if (item.transform.parent != _leftAreaTransform && item.transform.parent != _rightAreaTransform)
            {
                if (GameManager.Instance.GameState == GameState.Game)
                    GameManager.Instance.TaskCenter.CheckCenterDrop(item);
            }
            item.Release();
        }
    }
}