using UnityEngine;

public class OpenBoxRemover : MonoBehaviour
{
    private GameObject _lastDestroyedBox;

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Item>(out Item item))
        {
            item.Release();
        }
        else if (other.gameObject.CompareTag("OpenBox"))
        {
            if (_lastDestroyedBox != other.gameObject)
            {
                _lastDestroyedBox = other.gameObject;
                Destroy(other.gameObject);
                ItemSpawner.Instance.SpawnPackedBox();
            }
        }
        else Destroy(other.gameObject);
    }
}
