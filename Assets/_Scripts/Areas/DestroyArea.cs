using UnityEngine;

public class DestroyArea : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Item>(out Item item))
        {
            item.Release();
        }
        else if (other.gameObject.CompareTag("PackedBox"))
        {
            Destroy(other.gameObject);
        }
        else Destroy(other.gameObject);
    }
}
