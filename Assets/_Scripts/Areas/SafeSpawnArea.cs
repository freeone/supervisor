using UnityEngine;

public class SafeSpawnArea : MonoBehaviour
{
    private int _itemCount;
    private void OnTriggerEnter(Collider other)
    {
        _itemCount++;
    }

    private void OnTriggerExit(Collider other)
    {
        _itemCount--;
    }

    public bool IsEmpty()
    {
        return _itemCount == 0 ? true : false;
    }

    public void Clear()
    {
        _itemCount = 0;
    }
}
