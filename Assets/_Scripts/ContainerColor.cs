using UnityEngine;

public class ContainerColor : MonoBehaviour
{
    [SerializeField] private Material _leftContainerMaterial;
    [SerializeField] private Material _rightContainerMaterial;
    [SerializeField] private Color _defaultColor;
    [SerializeField] private Color _successColor;
    [SerializeField] private Color _failColor;

    private Material _materialToChange;

    public void ChangeContainerColor(Side side, ColorState color)
    {
        _materialToChange = side == Side.Left ? _leftContainerMaterial : _rightContainerMaterial;

        switch (color)
        {
            case ColorState.Success:
                _materialToChange.color = _successColor;
                break;
            case ColorState.Fail:
                _materialToChange.color = _failColor;
                break;
            default:
                _materialToChange.color = _defaultColor;
                break;
        }
    }

    public void ChangeBothColors(ColorState color)
    {
        ChangeContainerColor(Side.Left, color);
        ChangeContainerColor(Side.Right, color);
    }
}

public enum ColorState
{
    Default, Success, Fail
}
