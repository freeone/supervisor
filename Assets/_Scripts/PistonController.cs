using UnityEngine;

public class PistonController : MonoBehaviour
{
    [SerializeField] private Rigidbody _leftPiston;
    [SerializeField] private Rigidbody _rightPiston;
    [SerializeField] private float _forwardSpeed;
    [SerializeField] private float _retreatSpeed;
    [SerializeField] private BoxCollider _mainBeltCollider;

    private float _edgeValue = 0.6f;
    private Vector3 _leftStartPosition;
    private Vector3 _rightStartPosition;
    private float _inGapSpeedMultiplier = 1.5f;
    private float _mainBeltWidth;

    private bool _leftTouch;
    private bool _rightTouch;

    public bool IsLeftMoving { get; private set; }
    public bool IsRightMoving { get; private set; }

    public static PistonController Instance;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        _leftStartPosition = _leftPiston.transform.position;
        _rightStartPosition = _rightPiston.transform.position;
        _mainBeltWidth = _mainBeltCollider.bounds.max.x;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            StartCoroutine(BonusProcessor.UseBonus(BonusType.SlowBelt));
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            StartCoroutine(BonusProcessor.UseBonus(BonusType.IncreaseChance));
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            StartCoroutine(BonusProcessor.UseBonus(BonusType.Magnet));
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            StartCoroutine(BonusProcessor.UseBonus(BonusType.ShowNeeded));
        }
    }

    private void FixedUpdate()
    {
        MobileInput();

        if (GameManager.Instance.GameState == GameState.Game)
        {
            // Left Piston
            IsLeftMoving = false;

            if ((Input.GetKey(KeyCode.Q) || _leftTouch) && _leftPiston.transform.position.x < _edgeValue)
            {
                _leftPiston.MovePosition(_leftPiston.transform.position + Vector3.right * _forwardSpeed * CurrentGapSpeed(Side.Left) * Time.fixedDeltaTime);
                IsLeftMoving = true;
            }
            else if ((!Input.GetKey(KeyCode.Q) && !_leftTouch) && _leftPiston.transform.position.x > _leftStartPosition.x)
            {
                _leftPiston.MovePosition(_leftPiston.transform.position + Vector3.left * _retreatSpeed * CurrentGapSpeed(Side.Left) * Time.fixedDeltaTime);
                IsLeftMoving = true;
            }

            // Right Piston
            IsRightMoving = false;
            if ((Input.GetKey(KeyCode.E) || _rightTouch) && _rightPiston.transform.position.x > -_edgeValue)
            {
                _rightPiston.MovePosition(_rightPiston.transform.position + Vector3.left * _forwardSpeed * CurrentGapSpeed(Side.Right) * Time.fixedDeltaTime);
                IsRightMoving = true;
            }
            else if ((!Input.GetKey(KeyCode.E) && !_rightTouch) && _rightPiston.transform.position.x < _rightStartPosition.x)
            {
                _rightPiston.MovePosition(_rightPiston.transform.position + Vector3.right * _retreatSpeed * CurrentGapSpeed(Side.Right) * Time.fixedDeltaTime);
                IsRightMoving = true;
            }
        }        
    }

    private float CurrentGapSpeed(Side side)
    {
        if (side == Side.Left)
        {
            if (_leftPiston.transform.position.x < -_mainBeltWidth)
            {
                return _inGapSpeedMultiplier;
            }
        }
        else
        {
            if (_rightPiston.transform.position.x > _mainBeltWidth)
            {
                return _inGapSpeedMultiplier;
            }

        }
        return 1f;
    }

    private void MobileInput()
    {
        _leftTouch = false;
        _rightTouch = false;

        if (Input.touchCount > 0)
        {
            foreach (var touch in Input.touches)
            {
                if (touch.position.x < Screen.width / 2)
                {
                    _leftTouch = true;
                }
                if (touch.position.x > Screen.width / 2)
                {
                    _rightTouch = true;
                }
            }
        }
    }

}
