using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Flush), typeof(RewardCenter))]
public class GameManager : MonoBehaviour
{
    [SerializeField] private SideArea _leftSideArea;
    [SerializeField] private SideArea _rightSideArea;
    [SerializeField] private GameObject _leftMagnet;
    [SerializeField] private GameObject _rightMagnet;
    [SerializeField] private bool _canLoose;

    private GameDataManager _gameDataManager;
    private TaskCenter _taskCenter;
    private RewardCenter _rewardCenter;
    private GameState _gameState;
    private Flush _flush;
    private ContainerColor _containerColor;

    private Dictionary<Side, bool> _sendingOrderStates;
    public Dictionary<Side, bool> SendingOrderStates => _sendingOrderStates;

    public GameState GameState => _gameState;
    public TaskCenter TaskCenter => _taskCenter;

    public event Action OrderReCreated;
    public static GameManager Instance { get; private set; }

    private void Awake()
    {
        Instance = this;

        _gameDataManager = new GameDataManager();
        _rewardCenter = GetComponent<RewardCenter>();
        _flush = GetComponent<Flush>();
        _containerColor = GetComponent<ContainerColor>();
        
        _sendingOrderStates = new Dictionary<Side, bool> { { Side.Left, false }, { Side.Right, false } };

        _taskCenter = new TaskCenter();
    }

    private void Start()
    {
        BonusProcessor.Init(_leftMagnet, _rightMagnet);
        _gameState = GameState.Menu;
        UIManager.Instance.SetCoinsTextMainMenu(_gameDataManager.GameData.Coins);

        _containerColor.ChangeBothColors(ColorState.Default);

        _taskCenter.InitializeTaskLists();

        ItemSpawner.Instance.StartSpawner();
    }

    public void StartGame()
    {
        _rewardCenter.ResetRewards();
        ItemSpawner.Instance.ReleaseAllAtSpawnPoint();
        _containerColor.ChangeBothColors(ColorState.Default);

        _taskCenter.ResetAllTasks();
        TaskDisplay.Instance.ShowTaskList(_taskCenter.LeftStorageTasks, Side.Left);
        TaskDisplay.Instance.ShowTaskList(_taskCenter.RightStorageTasks, Side.Right);

        SwitchSideAreas(true);
        _gameState = GameState.Game;
    }

    public void LoseGame(LoseReason reason, ItemType type, Side side = Side.Left)
    {
        if (_gameState == GameState.Game && _canLoose)
        {
            SwitchSideAreas(false);
            _gameState = GameState.GameOver;

            bool newRecord = _gameDataManager.CheckNewRecord(_rewardCenter.CurrentBoxes);
            UIManager.Instance.LoseGame(reason, type, _rewardCenter, _containerColor, newRecord, side);

            if (_rewardCenter.CurrentBoxes != 0)
            {
                _gameDataManager.TryAddNewScore(_rewardCenter.CurrentBoxes);
                _gameDataManager.GameData.AddCoins(_rewardCenter.CurrentCoins);
                _gameDataManager.SaveData();
            }

            Debug.Log("Coins total: " + _gameDataManager.GameData.Coins);
            foreach (var item in _gameDataManager.GameData.Scores)
            {
                Debug.Log(item);
            }
        }
    }

    public void RestartGame()
    {
        _flush.FlushBoth();
        _containerColor.ChangeBothColors(ColorState.Default);

        StartCoroutine(ChangingBox(Side.Left));
        StartCoroutine(ChangingBox(Side.Right));

        StartGame();
    }

    public void OpenMenu()
    {
        UIManager.Instance.SetCoinsTextMainMenu(_gameDataManager.GameData.Coins);
        SwitchSideAreas(false);
        _gameState = GameState.Menu;

        _flush.FlushBoth();
        _containerColor.ChangeBothColors(ColorState.Default);

        StartCoroutine(ChangingBox(Side.Left));
        StartCoroutine(ChangingBox(Side.Right));
    }

    private void SwitchSideAreas(bool newState)
    {
        if (newState)
        {
            _leftSideArea.enabled = true;
            _rightSideArea.enabled = true;
        }
        else
        {
            _leftSideArea.enabled = false;
            _rightSideArea.enabled = false;
        }
    }

    public void SendOrder(Side side)
    {
        StartCoroutine(SendingOrder(side));
    }

    private IEnumerator SendingOrder(Side side)
    {
        var (lines, totalCount) = TaskCenter.GetTaskInfo(side);
        int reward = _rewardCenter.GetRewardForOrder(lines, totalCount);
        Debug.Log(reward); // show it bright
        _rewardCenter.ShowReward(side, reward);

        _sendingOrderStates[side] = true;

        _containerColor.ChangeContainerColor(side, ColorState.Success);

        TaskDisplay.Instance.SwitchColorToCompleted(side, true);
        Belts.Instance.MoveMainBeltBackward();
        _flush.FlushOne(side);

        yield return new WaitForSeconds(1.5f);

        Transform newBox = ItemSpawner.Instance.SpawnEmptyBox(side);
        Belts.Instance.DeliverBox(newBox, side);

        TaskDisplay.Instance.SwitchColorToCompleted(side, false);
        _taskCenter.ResetSideTasks(side);
        if (side == Side.Left)
            TaskDisplay.Instance.ShowTaskList(_taskCenter.LeftStorageTasks, side);
        else 
            TaskDisplay.Instance.ShowTaskList(_taskCenter.RightStorageTasks, side);
        OrderReCreated?.Invoke();

        _sendingOrderStates[side] = false;

        Side anotherSide = side == Side.Left ? Side.Right : Side.Left;
        if (!_sendingOrderStates[anotherSide])
            Belts.Instance.MoveMainBeltForward();

        _containerColor.ChangeContainerColor(side, ColorState.Default);
    }

    private IEnumerator ChangingBox(Side side)
    {
        yield return new WaitForSeconds(1.5f);

        Transform newBox = ItemSpawner.Instance.SpawnEmptyBox(side);
        Belts.Instance.DeliverBox(newBox, side);
    }


}


public enum GameState
{
    Menu, Game, GameOver
}

public enum LoseReason
{
    WrongType, TooMany, DroppedNeeded
}