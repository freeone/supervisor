using UnityEngine;
using TMPro;
using System.Collections;

public class RewardCenter : MonoBehaviour
{
    [SerializeField] private TextMeshPro _leftStorageRewardText;
    [SerializeField] private TextMeshPro _rightStorageRewardText;
    [SerializeField] private TextMeshPro _leftStorageBoxText;
    [SerializeField] private TextMeshPro _rightStorageBoxText;

    private int _currentCoins = 0;
    private int _currentBoxes = 0;
    private int _currentLeftBoxes = 0;
    private int _currentRightBoxes = 0;

    public int CurrentCoins => _currentCoins;
    public int CurrentBoxes => _currentBoxes;

    public void ResetRewards()
    {
        _currentCoins = 0;
        _currentBoxes = 0;

        _currentLeftBoxes = 0;
        _currentRightBoxes = 0;
        UpdateBoxText(Side.Left);
        UpdateBoxText(Side.Right);
}

    public int GetRewardForOrder(int linesInOrder, int totalOrderCount)
    {
        int newCoins = (2 + linesInOrder) * totalOrderCount + _currentBoxes;
        _currentCoins += newCoins;
        _currentBoxes++;

        return newCoins;
    }

    public void ShowReward(Side side, int coins)
    {
        TextMeshPro rewardText = side == Side.Left ? _leftStorageRewardText : _rightStorageRewardText;

        rewardText.text = coins < 99 ? $"+{coins}" : coins.ToString();
        rewardText.color = Color.white;
        StartCoroutine(FadeOut(2, rewardText));

        if (side == Side.Left) _currentLeftBoxes++;
        else _currentRightBoxes++;
        UpdateBoxText(side);
    }

    public void UpdateBoxText(Side side)
    {
        TextMeshPro rewardText = side == Side.Left ? _leftStorageBoxText : _rightStorageBoxText;

        rewardText.text = side == Side.Left ? _currentLeftBoxes.ToString() : _currentRightBoxes.ToString();
    }

    private IEnumerator FadeOut(float duration, TextMeshPro rewardText)
    {
        float currentTime = 0f;
        while (currentTime < duration)
        {
            float alpha = Mathf.Lerp(1f, 0f, currentTime / duration);
            rewardText.color = new Color(rewardText.color.r, rewardText.color.g, rewardText.color.b, alpha);
            currentTime += Time.deltaTime;
            yield return null;
        }
        yield break;
    }
}
